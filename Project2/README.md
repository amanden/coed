# Project 2
# Multi-Party Computation

# Task 1
The files for task 1 are in task1 folder and they are:
```bash
docker-compose.yml
Dockerfile
peers.py
```
To build the docker images and run the code in containers, go to the folder /task1 and run the command:
```bash
docker-compose up --build 
```
# Task 2
The files for task 2 are added in task2:
```bash
docker-compose.yml
Dockerfile
peers.py
```
To build the docker images and run the code in containers, go to the folder /task2 and run the command:
```bash
docker-compose up --build 
```
# Task 3
The files for task 3 are added in task3:
```bash
docker-compose.yml
Dockerfile
peers.py
```
To build the docker images and run the code in containers, go to the folder /task3 and run the command:
```bash
docker-compose up --build 
```
