"""
 Author: Aswathi Manden (ak3793)
 
 Task 1:
 Alice has a secret which is 3. She makes the shares and sends it to all the other parties.
 The polynomial degree (t) used is 2

"""

import socket
import sys
import threading
import time
import random

HEADER = 2048
PORT = 5050
FORMAT = 'utf-8'

# The coefficients a0, a1 and a2 for the polynomial.
# The secret is hidden at f(0) which is 3
a = [3 , random.getrandbits(80), random.getrandbits(80)]

count = 1

# Secret share received from Alice
sec_share = []

# List of secret shares received from all the other parties
f_x =[]

# List of client ids
x = []

"""
 The main function of execution
 
"""
def main():
    my_name = sys.argv[1] #Alice
    my_id = sys.argv[2] #1
    my_secret = sys.argv[3]  #11
    peers = ["Alice", "Bob" ,"Charlie", "David","Eve"]
    primeModP = 313
    
    thread = threading.Thread(target=receive, args=(my_name,))
    thread.start()
    time.sleep(1)
    
    if(my_name == "Alice"):
        i = 1
        for peer in peers:
            y = a[2] * i ** 2 + a[1] * i + a[0]
            y = y % primeModP
            send(peer, y)
            i += 1
            
    time.sleep(2)
    if(len(sec_share) == 1):
        for peer in peers:
            send(peer, "f("+my_id+"): " + str(sec_share[0]))
            
    time.sleep(2)
    if(my_name == "Eve"):
        print("Eve reconstructing secret share ...")
        reconstruct(x, f_x, primeModP)

"""
 The function for reconstructing the ssecret from t+1 shares
 
"""
def reconstruct(x, f_x, primeModP):
    if(len(x) >= 3):
        s = ((x[0] * x[1] * (x[0] - x[1]) * f_x[2]) + \
             (x[1] * x[2] * (x[1] - x[2]) * f_x[0]) + \
             (x[2] * x[0] * (x[2] - x[0]) * f_x[1])) / \
            ((x[2] * x[2] - x[2] * (x[1] + x[0]) + x[0] * x[1]) * (x[0] - x[1]))
        s = s % primeModP
        print("The secret value is :", s)

"""
 The function for sending messages
 
"""
def send(name, msg):
    serv = socket.gethostbyname(name)
    ADDR = (serv, 8000)
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    message = (str(msg)).encode(FORMAT)
    client.sendto(message, ADDR)
    client.close()

"""
 The function for receiving messages
 
"""
def receive(my_name):
    SERVER = socket.gethostbyname(my_name)
    ADDR = (SERVER, 8000)
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.bind(ADDR)
    global count
    while count<7:
        data, addr = soc.recvfrom(2048)
        count += 1
        msg = data.decode(FORMAT)
   
        if( msg.isnumeric()):
            sec_share.append(int(msg))
            
        elif(msg[:1] == 'f'):
            share = int(msg[6:])
            client_id = int(msg[2:3])
            x.append(client_id)
            f_x.append(share)
    
    count = 1
    soc.close()


main()
