"""
 Task 3:
 The program to calculate the product of all private values

"""

import socket
import sys
import threading
import time
import random

HEADER = 2048
FORMAT = 'utf-8'

count = 1
x = [0]
y = [0]
a = [0]
b = [0]
c = [0]
xa = [0] * 5
yb = [0] * 5
z = [0] * 5


"""
 The function of sending messages

"""
def send(name, msg):
    serv = socket.gethostbyname(name)
    ADDR = (serv, 8000)
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    message = (str(msg)).encode(FORMAT)
    client.sendto(message, ADDR)
    client.close()


"""
 This function generates the polynomials of degree 2 (t = 2) with the secret

"""
def generate_polynomial(coef, id):
    p = 313
    f = coef[2] * id * id + coef[1] * id + coef[0]
    f = f % p
    return f


"""
This function reconstructs the secret with t + 1 shares (3 shares)
"""
def reconstruct(x, y):

    p = 313
    t1 = (x[0] * x[1] * (x[0] - x[1]) * y[2])
    t2 = (x[1] * x[2] * (x[1] - x[2]) * y[0])
    t3 = (x[2] * x[0] * (x[2] - x[0]) * y[1])
    deno = (x[2] * x[2] - x[2] * (x[1] + x[0]) + x[0] * x[1]) * (x[0] - x[1])

    total = ((t1 + t2 + t3) / deno) % p
    return total


"""
 The function of receiving messages

"""
def receive(my_name):
    SERVER = socket.gethostbyname(my_name)
    ADDR = (SERVER, 8000)
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.bind(ADDR)
    global count
    while count < 20:
        data, addr = soc.recvfrom(2048)
        count += 1
        data = data.decode(FORMAT)

        if(data[:1] == 'a'):
            a[0] = (int(data[2:]))
        elif(data[:1] == 'b'):
            b[0] = (int(data[2:]))
        elif(data[:1] == 'c'):
            c[0] = (int(data[2:]))
        elif(data[:1] == 'x'):
            id = int(data[1:2])
            xa[id-1] = (int(data[3:]))
        elif(data[:1] == 'y'):
            id = int(data[1:2])
            yb[id-1] = (int(data[3:]))
        elif(data[:1] == 'z'):
            id = int(data[1:2])
            z[id-1] = (float(data[3:]))
        else:
            id = int(data[:1])
            sec_share = int(data[2:])
            if(id == 2):
                y[0] = sec_share
            if(id == 1):
                x[0] = sec_share
    count = 1
    soc.close()


"""
 The main function of execution

"""


def main():
    my_name = sys.argv[1]  # Alice
    my_secret = sys.argv[2]  # 11

    coef = [int(my_secret), random.getrandbits(80), random.getrandbits(80)]

    if (my_name != "dealer"):
        thread = threading.Thread(target=receive, args=(my_name,))
        thread.start()
        time.sleep(2)

    if (my_name == "Alice"):
        x[0] = generate_polynomial(coef, 1)
        send("Bob", "1_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "1_" + str(generate_polynomial(coef, 3)))
        send("David", "1_" + str(generate_polynomial(coef, 4)))
        send("Eve", "1_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Bob"):
        send("Alice", "2_" + str(generate_polynomial(coef, 1)))
        y[0] = generate_polynomial(coef, 2)
        send("Charlie", "2_" + str(generate_polynomial(coef, 3)))
        send("David", "2_" + str(generate_polynomial(coef, 4)))
        send("Eve", "2_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Charlie"):
        send("Alice", "3_" + str(generate_polynomial(coef, 1)))
        send("Bob", "3_" + str(generate_polynomial(coef, 2)))
        send("David", "3_" + str(generate_polynomial(coef, 4)))
        send("Eve", "3_" + str(generate_polynomial(coef, 5)))

    if (my_name == "David"):
        send("Alice", "4_" + str(generate_polynomial(coef, 1)))
        send("Bob", "4_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "4_" + str(generate_polynomial(coef, 3)))
        send("Eve", "4_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Eve"):
        send("Alice", "5_" + str(generate_polynomial(coef, 1)))
        send("Bob", "5_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "5_" + str(generate_polynomial(coef, 3)))
        send("David", "5_" + str(generate_polynomial(coef, 4)))

    ai = 3
    bi = 5
    if (my_name == "dealer"):
        time.sleep(5)
        coef = [ai, random.getrandbits(80), random.getrandbits(80)]
        send("Alice", "a_" + str(generate_polynomial(coef, 1)))
        send("Bob", "a_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "a_" + str(generate_polynomial(coef, 3)))
        send("David", "a_" + str(generate_polynomial(coef, 4)))
        send("Eve", "a_" + str(generate_polynomial(coef, 5)))

        coef = [bi, random.getrandbits(80), random.getrandbits(80)]
        send("Alice", "b_" + str(generate_polynomial(coef, 1)))
        send("Bob", "b_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "b_" + str(generate_polynomial(coef, 3)))
        send("David", "b_" + str(generate_polynomial(coef, 4)))
        send("Eve", "b_" + str(generate_polynomial(coef, 5)))

        ci = ai * bi
        coef = [ci, random.getrandbits(80), random.getrandbits(80)]
        send("Alice", "c_" + str(generate_polynomial(coef, 1)))
        send("Bob", "c_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "c_" + str(generate_polynomial(coef, 3)))
        send("David", "c_" + str(generate_polynomial(coef, 4)))
        send("Eve", "c_" + str(generate_polynomial(coef, 5)))
    
    time.sleep(5)
    
    coef = [x[0]-a[0], random.getrandbits(80), random.getrandbits(80)]
    if (my_name == "Alice"):
        xa[0] = generate_polynomial(coef, 1)
        send("Bob", "x1_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "x1_" + str(generate_polynomial(coef, 3)))
        send("David", "x1_" + str(generate_polynomial(coef, 4)))
        send("Eve", "x1_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Bob"):
        send("Alice", "x2_" + str(generate_polynomial(coef, 1)))
        xa[1] = generate_polynomial(coef, 2)
        send("Charlie", "x2_" + str(generate_polynomial(coef, 3)))
        send("David", "x2_" + str(generate_polynomial(coef, 4)))
        send("Eve", "x2_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Charlie"):
        send("Alice", "x3_" + str(generate_polynomial(coef, 1)))
        send("Bob", "x3_" + str(generate_polynomial(coef, 2)))
        xa[2] = generate_polynomial(coef, 3)
        send("David", "x3_" + str(generate_polynomial(coef, 4)))
        send("Eve", "x3_" + str(generate_polynomial(coef, 5)))

    if (my_name == "David"):
        send("Alice", "x4_" + str(generate_polynomial(coef, 1)))
        send("Bob", "x4_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "x4_" + str(generate_polynomial(coef, 3)))
        xa[3] = generate_polynomial(coef, 4)
        send("Eve", "x4_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Eve"):
        send("Alice", "x5_" + str(generate_polynomial(coef, 1)))
        send("Bob", "x5_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "x5_" + str(generate_polynomial(coef, 3)))
        send("David", "x5_" + str(generate_polynomial(coef, 4)))
        xa[4] = generate_polynomial(coef, 5)

    time.sleep(5)
    
    coef = [y[0]-b[0], random.getrandbits(80), random.getrandbits(80)]
    if (my_name == "Alice"):
        yb[0] = generate_polynomial(coef, 1)
        send("Bob", "y1_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "y1_" + str(generate_polynomial(coef, 3)))
        send("David", "y1_" + str(generate_polynomial(coef, 4)))
        send("Eve", "y1_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Bob"):
        send("Alice", "y2_" + str(generate_polynomial(coef, 1)))
        yb[1] = generate_polynomial(coef, 2)
        send("Charlie", "y2_" + str(generate_polynomial(coef, 3)))
        send("David", "y2_" + str(generate_polynomial(coef, 4)))
        send("Eve", "y2_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Charlie"):
        send("Alice", "y3_" + str(generate_polynomial(coef, 1)))
        send("Bob", "y3_" + str(generate_polynomial(coef, 2)))
        yb[2] = generate_polynomial(coef, 3)
        send("David", "y3_" + str(generate_polynomial(coef, 4)))
        send("Eve", "y3_" + str(generate_polynomial(coef, 5)))

    if (my_name == "David"):
        send("Alice", "y4_" + str(generate_polynomial(coef, 1)))
        send("Bob", "y4_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "y4_" + str(generate_polynomial(coef, 3)))
        yb[3] = generate_polynomial(coef, 4)
        send("Eve", "y4_" + str(generate_polynomial(coef, 5)))

    if (my_name == "Eve"):
        send("Alice", "y5_" + str(generate_polynomial(coef, 1)))
        send("Bob", "y5_" + str(generate_polynomial(coef, 2)))
        send("Charlie", "y5_" + str(generate_polynomial(coef, 3)))
        send("David", "y5_" + str(generate_polynomial(coef, 4)))
        yb[4] = generate_polynomial(coef, 5)

    time.sleep(5)
    
    if(my_name != "dealer"):
        ilist = [3, 2 ,1]
        xlist = [xa[2], xa[1], xa[0]]
        ylist = [yb[2], yb[1] , yb[0] ]
        x_prime = reconstruct(ilist, xlist)
        y_prime = reconstruct(ilist, ylist)
        
        zi = c[0] + x_prime * b[0] + y_prime * a[0] + 2 * (x_prime * y_prime)
        
        print("The zi values :",zi)
        
        coef = [zi, random.getrandbits(80), random.getrandbits(80)]
        if (my_name == "Alice"):
            z[0] = generate_polynomial(coef, 1)
            send("Bob", "z1_" + str(generate_polynomial(coef, 2)))
            send("Charlie", "z1_" + str(generate_polynomial(coef, 3)))
            send("David", "z1_" + str(generate_polynomial(coef, 4)))
            send("Eve", "z1_" + str(generate_polynomial(coef, 5)))

        if (my_name == "Bob"):
            send("Alice", "z2_" + str(generate_polynomial(coef, 1)))
            z[1] = generate_polynomial(coef, 2)
            send("Charlie", "z2_" + str(generate_polynomial(coef, 3)))
            send("David", "z2_" + str(generate_polynomial(coef, 4)))
            send("Eve", "z2_" + str(generate_polynomial(coef, 5)))

        if (my_name == "Charlie"):
            send("Alice", "z3_" + str(generate_polynomial(coef, 1)))
            send("Bob", "z3_" + str(generate_polynomial(coef, 2)))
            z[2] = generate_polynomial(coef, 3)
            send("David", "z3_" + str(generate_polynomial(coef, 4)))
            send("Eve", "z3_" + str(generate_polynomial(coef, 5)))

        if (my_name == "David"):
            send("Alice", "z4_" + str(generate_polynomial(coef, 1)))
            send("Bob", "z4_" + str(generate_polynomial(coef, 2)))
            send("Charlie", "z4_" + str(generate_polynomial(coef, 3)))
            z[3] = generate_polynomial(coef, 4)
            send("Eve", "z4_" + str(generate_polynomial(coef, 5)))

        if (my_name == "Eve"):
            send("Alice", "z5_" + str(generate_polynomial(coef, 1)))
            send("Bob", "z5_" + str(generate_polynomial(coef, 2)))
            send("Charlie", "z5_" + str(generate_polynomial(coef, 3)))
            send("David", "z5_" + str(generate_polynomial(coef, 4)))
            z[4] = generate_polynomial(coef, 5)
            
            time.sleep(5)
            zlist = [z[2], z[1], z[0]]
            prod = reconstruct(ilist, zlist)
            
            print("The product of shares are :", prod)
    
main()

