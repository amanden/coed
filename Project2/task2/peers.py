"""
 The program to calculate the sum of all private values
 
"""

import socket
import sys
import threading
import time
import random

HEADER = 2048
FORMAT = 'utf-8'

#SERVER = socket.gethostbyname(socket.gethostname())

count = 1
share_recv = [0] * 5
sum_list = [0] * 5

"""
 The function of sending messages
 
"""
def send(name, msg):
    serv = socket.gethostbyname(name)
    ADDR = (serv, 8000)
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    message = (str(msg)).encode(FORMAT)
    client.sendto(message, ADDR)
    client.close()

"""
 The function of receiving messages
 
"""
def receive(my_name):
    SERVER = socket.gethostbyname(my_name)
    ADDR = (SERVER, 8000)
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.bind(ADDR)
#    soc.listen()
    global count
    while count<5:
        data, addr = soc.recvfrom(2048)
        count += 1
        data = data.decode(FORMAT)
        
        if(data[:1] == 's'):
            id = int(data[1:2])
            sum = int(data[3:])
            sum_list[id-1] = sum
        else:
            id = int(data[:1])
            sec_share = int(data[2:])
            share_recv[id-1] = sec_share
    count = 1
    soc.close()

"""
 This function generates the polynomials of degree 2 (t = 2) with the secret
 
"""
def generate_polynomial(a, id, my_secret):
    p = 313
    f = a[2] * id * id + a[1] * id + a[0]
    f = f%p
    return f

       
"""
 This function reconstructs the secret with t + 1 shares (3 shares)
 
"""
def send_secret_shares(my_name, my_secret, a):
    if(my_name == "Alice"):
        share_recv[0] = generate_polynomial(a, 1, my_secret)
        send("Bob", "1_"+str(generate_polynomial(a, 2, my_secret)) )
        send("Charlie", "1_"+str(generate_polynomial(a, 3, my_secret)) )
        send("David", "1_"+str(generate_polynomial(a, 5, my_secret)) )
        send("Eve", "1_"+str(generate_polynomial(a, 6, my_secret)) )

    if(my_name == "Bob"):
        send("Alice", "2_"+str(generate_polynomial(a, 1, my_secret)))
        share_recv[1] = generate_polynomial(a, 2, my_secret)
        send("Charlie", "2_"+str(generate_polynomial(a,3 , my_secret)) )
        send("David", "2_"+str(generate_polynomial(a, 4, my_secret)) )
        send("Eve", "2_"+str(generate_polynomial(a, 5, my_secret)) )
        
    if(my_name == "Charlie"):
        send("Alice", "3_"+str(generate_polynomial(a, 1, my_secret)) )
        send("Bob", "3_"+str(generate_polynomial(a, 2, my_secret)) )
        share_recv[2] = generate_polynomial(a, 3, my_secret)
        send("David", "3_"+str(generate_polynomial(a,4, my_secret)) )
        send("Eve", "3_"+str(generate_polynomial(a,5, my_secret)) )
        
    if(my_name == "David"):
        send("Alice", "4_"+str(generate_polynomial(a,1, my_secret)) )
        send("Bob", "4_"+str(generate_polynomial(a,2, my_secret)) )
        send("Charlie", "4_"+str(generate_polynomial(a,3, my_secret)) )
        share_recv[3] = generate_polynomial(a,4, my_secret)
        send("Eve", "4_"+str(generate_polynomial(a,5, my_secret)) )
        
    if(my_name == "Eve"):
        send("Alice", "5_"+str(generate_polynomial(a,1, my_secret)) )
        send("Bob", "5_"+str(generate_polynomial(a,2, my_secret)) )
        send("Charlie", "5_"+str(generate_polynomial(a,3, my_secret)) )
        send("David", "5_"+str(generate_polynomial(a,4, my_secret)) )
        share_recv[4] = generate_polynomial(a,5, my_secret)
    
    
    time.sleep(5)
    print("Shares received  :" , share_recv)

"""
 The function to send the polynomial sum
 
"""
def send_sum(my_name, su_mm):
    if(my_name == "Alice"):
        sum_list[0] = su_mm
        send("Bob", "s1_"+str(su_mm) )
        send("Charlie", "s1_"+str(su_mm) )
        send("David", "s1_"+str(su_mm) )
        send("Eve", "s1_"+str(su_mm) )

    if(my_name == "Bob"):
        send("Alice", "s2_"+str(su_mm)  )
        sum_list[1] = su_mm
        send("Charlie", "s2_"+str(su_mm)  )
        send("David", "s2_"+str(su_mm) )
        send("Eve", "s2_"+str(su_mm) )
        
    if(my_name == "Charlie"):
        send("Alice", "s3_"+str(su_mm) )
        send("Bob", "s3_"+str(su_mm) )
        sum_list[2] = su_mm
        send("David", "s3_"+str(su_mm) )
        send("Eve", "s3_"+str(su_mm) )
        
    if(my_name == "David"):
        send("Alice", "s4_"+str(su_mm) )
        send("Bob", "s4_"+str(su_mm) )
        send("Charlie", "s4_"+str(su_mm) )
        sum_list[3] = su_mm
        send("Eve", "s4_"+str(su_mm))
        
    if(my_name == "Eve"):
        send("Alice", "s5_"+str(su_mm) )
        send("Bob", "s5_"+str(su_mm)  )
        send("Charlie", "s5_"+str(su_mm)  )
        send("David", "s5_"+str(su_mm)  )
        sum_list[4] = su_mm
        
    time.sleep(2)
    print("sums  :" , sum_list)

    
"""
This function reconstructs the secret with t + 1 shares (3 shares)
"""
def reconstruct(x, y):
#    s = ((x[0] * x[1] * (x[0] - x[1]) * y[2]) + (x[1] * x[2] * (x[1] - x[2]) * y[0]) + (x[2] * x[0] * (x[2] - x[0]) * y[1])) / (
#        (x[2] * x[2] - x[2] * (x[1] + x[0]) + x[0] * x[1]) * (
#            x[0] - x[1]))
    p = 313
    t1 = (x[0] * x[1] * (x[0] - x[1]) * y[2])
    t2 = (x[1] * x[2] * (x[1] - x[2]) * y[0])
    t3 = (x[2] * x[0] * (x[2] - x[0]) * y[1])
    deno = (x[2] * x[2] - x[2] * (x[1] + x[0]) + x[0] * x[1]) * (x[0] - x[1])
    print()
    print("Total sum :", ((t1 + t2 +t3 )/deno)%p)
    print()
    
    
"""
 The main function of execution
 
"""
def main():
    my_name = sys.argv[1] #Alice
    my_secret = sys.argv[2]  #11
    print("Client name :",my_name)

    thread = threading.Thread(target=receive, args=( my_name, ))
    thread.start()
    time.sleep(2)

    a = [int(my_secret), random.getrandbits(80), random.getrandbits(80)]
    
    send_secret_shares(my_name, my_secret, a)
    
    su_mm = sum(share_recv)
    print("Share of sums received  :",su_mm)
    
    thread = threading.Thread(target=receive, args=( my_name, ))
    thread.start()
    time.sleep(2)
    
    send_sum(my_name, su_mm)
    
    if(my_name == "Eve"):
        x = [3, 2 ,1]
        y = [sum_list[2], sum_list[1] , sum_list[0] ]
        reconstruct(x, y)
    
main()

