//
// Created by Aswathi Kunnumbrath Manden(ak3793)
// 10/14/20
// The program homomorphically evaluate multiplication of two 3x3 matrices
//

#ifndef HELIBEXAMPLES_MATRIXMULTIPLICATION_H
#define HELIBEXAMPLES_MATRIXMULTIPLICATION_H

#include "Encryptor.h"

class MatrixMultiplication {
public:
    // Performs the matrix multiplication homomorphically on encrypted data
    static void matrix_product(std::vector<std::vector<long>> &v_1, std::vector<std::vector<long>> &v_2);

private:
    // This function is used by debug function to print the plaintext vector
    static void print_vector(const std::string &msg, const std::vector<long> &vec, bool space=false);

    // This function is used to print the input matrices
    static void print_input_vector(const std::string &msg, const std::vector<std::vector<long>> &vec);

    //This function is used for debugging. Decryptes the encrypted ciphertext and prints the plaintext.
    static void debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space=false);

};


#endif //HELIBEXAMPLES_MATRIXMULTIPLICATION_H
