//
// Created by Aswathi Kunnumbrath Manden(ak3793)
// 10/14/20
// The program homomorphically evaluate dot product of two 5x5 matrices
//

#include "DotProduct.h"
#include <iostream>
#include "Util.h"

void
DotProduct::print_vector(const std::string &msg, const std::vector<long> &vec, bool space) {
    std::cout << msg;
    for (auto &slot : vec) {
        std::cout << slot;
        if (space)
            std::cout << " ";
    }
    std::cout << std::endl;
}
void
DotProduct::print_input_vector(const std::string &msg, const std::vector<long> &vec) {
    COED::Util::info(msg);
    for(int i = 0; i < 5; i++){
        std::cout << vec[i] << " ";
    }
    std::cout << std::endl;
}

void
DotProduct::debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space) {
    std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
    encryptor.getEncryptedArray()->decrypt(ctxt, *encryptor.getSecretKey(), plaintext);
    print_vector(msg, plaintext, space);
}
void
DotProduct::fill_plaintext_with_numbers(std::vector<long> &plaintext, std::vector<long> &v) {
    for(int i=0; i < v.size(); ++i) {
        plaintext[i] = v[i];
    }
    for(int i=v.size(); i<plaintext.size(); ++i) {
        plaintext[i] = 0;
    }
}

/**
 * performs the dot product of 2 5x5 vectors
 * encoded the input vectors into 2 ciphertexts and used the totalsum function to
 * calculate the dot product homomorphically
 * @param v_1 input vector 1
 * @param v_2 input vector 2
 */
void
DotProduct::dot_product(std::vector<long> v_1, std::vector<long> v_2) {
    unsigned long plaintext_prime_modulus = 53;
    unsigned long phiM = 299;
    unsigned long lifting = 1;
    unsigned long numOfBitsOfModulusChain = 512;
    unsigned long numOfColOfKeySwitchingMatrix = 2;

    COED::Encryptor encryptor("/tmp/sk.txt", "/tmp/pk.txt",
                              plaintext_prime_modulus,
                              phiM,
                              lifting,
                              numOfBitsOfModulusChain,
                              numOfColOfKeySwitchingMatrix);
    COED::Util::info("Finished creating encryptor.");

    print_input_vector("Input vector 1:", v_1);
    print_input_vector("Input vector 2:", v_2);

    // plaintext vectors
    std::vector<long> vec_1(encryptor.getEncryptedArray()->size()), vec_2(encryptor.getEncryptedArray()->size());

    fill_plaintext_with_numbers(vec_1, v_1);
    fill_plaintext_with_numbers(vec_2, v_2);

    COED::Util::info("Finished creating plaintext.\n");
    helib::Ctxt vec_1_ctxt(*encryptor.getPublicKey()), vec_2_ctxt(*encryptor.getPublicKey());

    //  Encrypt the plaintext using the public_key - ciphertext 1
    encryptor.getEncryptedArray()->encrypt(vec_1_ctxt, *encryptor.getPublicKey(), vec_1);
    debug(encryptor, vec_1_ctxt, "Plaintext vector 1: ", true);

    // Encrypt the plaintext using the public_key - ciphertext 2
    encryptor.getEncryptedArray()->encrypt(vec_2_ctxt, *encryptor.getPublicKey(), vec_2);
    debug(encryptor, vec_2_ctxt, "Plaintext vector 2: ", true);

    // performing the dot product of 2 vectors using the totalsums function
    helib::Ctxt product_ctxt(vec_1_ctxt);
    product_ctxt.multiplyBy(vec_2_ctxt);
    helib::totalSums(*encryptor.getEncryptedArray(), product_ctxt);

//    debug(encryptor, product_ctxt, "Dot product ciphertext: ", true);  // print this to verify the output ciphertext
    std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
    // decrypting the output ciphertext and displaying the result
    encryptor.getEncryptedArray()->decrypt(product_ctxt, *encryptor.getSecretKey(), plaintext);
    std::cout << "Dot product: " << plaintext[0] <<"\n" << std::endl;

}
