//
// Created by Aswathi Kunnumbrath Manden(ak3793)
// 10/14/20
// The program homomorphically evaluate multiplication of two 3x3 matrices
//

#include "MatrixMultiplication.h"
#include <iostream>
#include "Util.h"

void
MatrixMultiplication::print_vector(const std::string &msg, const std::vector<long> &vec, bool space) {
    std::cout << msg;
    for (auto &slot : vec) {
        std::cout << slot;
        if (space)
            std::cout << " ";
    }
    std::cout << std::endl;
}

void
MatrixMultiplication::print_input_vector(const std::string &msg, const std::vector<std::vector<long>> &vec) {
    std::cout << msg << std::endl;
    for(int i = 0; i < vec.size(); i++){
        for (int j = 0; j < vec[0].size(); j++) {
            std::cout << vec[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void
MatrixMultiplication::debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space) {
    std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
    encryptor.getEncryptedArray()->decrypt(ctxt, *encryptor.getSecretKey(), plaintext);
    print_vector(msg, plaintext, space);
}

/**
 *  Matrix is represented as 2D vector.
 *  For performing the matrix multiplication, encoded each row in matrix 1 into plaintexts and
 *  encoded each column in matrix 2 as plaintexts.
 *  3x3 matrix is converted as 6 ciphertexts.
 *  9 Dot product operations are done on these 6 iphertexts to get the output matrix.
 *  @arg v_1 :input matrix 1
 *  @arg v_2 :input matrix 2
 *
 */
void
MatrixMultiplication::matrix_product(std::vector<std::vector<long>> &v_1, std::vector<std::vector<long>> &v_2) {
    unsigned long plaintext_prime_modulus = 53;
    unsigned long phiM = 313;
    unsigned long lifting = 1;
    unsigned long numOfBitsOfModulusChain = 512;
    unsigned long numOfColOfKeySwitchingMatrix = 2;

    COED::Encryptor encryptor("/tmp/sk.txt", "/tmp/pk.txt",
                              plaintext_prime_modulus,
                              phiM,
                              lifting,
                              numOfBitsOfModulusChain,
                              numOfColOfKeySwitchingMatrix);
    COED::Util::info("Finished creating encryptor.");

    print_input_vector("Matrix 1: ", v_1);
    print_input_vector("Matrix 2: ", v_2);

    // created 2 vectors of plaintexts for each input matrix
    // in1_vec_pt: vector of 3 plaintexts corresponding to matrix 1
    // in2_vec_pt: vector of 3 plaintexts corresponding to matrix 2
    std::vector<std::vector<long>> in1_vec_pt, in2_vec_pt;
    for(int i = 0; i < 3; i++) {
        std::vector<long> vec_1(encryptor.getEncryptedArray()->size()), vec_2(encryptor.getEncryptedArray()->size());
        for (int j = 0; j < 3; j++) {
            vec_1[j] = v_1[i][j];  // rows in the matrix 1 are encoded into plaintext
            vec_2[j] = v_2[j][i];  // columns in the matrix 2 are encoded into plaintext
        }
        for(int i=v_1.size(); i<vec_1.size(); ++i) {
            vec_1[i] = 0;
            vec_2[i] = 0;
        }
        in1_vec_pt.push_back(vec_1);
        in2_vec_pt.push_back(vec_2);
    }

    COED::Util::info("Finished creating plaintext.");
    print_input_vector("Matrix 1 plaintext: ", in1_vec_pt);
    print_input_vector("Matrix 2 plaintext: ", in2_vec_pt);

    // created 2 vectors of ciphertexts for each plaintext
    // vec_ctxt1: vector of 3 ciphertexts corresponding to plaintext of matrix 1
    // vec_ctxt2: vector of 3 plaintexts corresponding to plaintext of matrix 2
    std::vector<helib::Ctxt> vec_ctxt1, vec_ctxt2;
    for(int i = 0; i < 3; i++) {
        helib::Ctxt ctxt(*encryptor.getPublicKey());
        encryptor.getEncryptedArray()->encrypt(ctxt, *encryptor.getPublicKey(), in1_vec_pt[i]);
        vec_ctxt1.push_back(ctxt);

        helib::Ctxt ctxt2(*encryptor.getPublicKey());
        encryptor.getEncryptedArray()->encrypt(ctxt2, *encryptor.getPublicKey(), in2_vec_pt[i]);
        vec_ctxt2.push_back(ctxt2);
    }

    // performs the 9 dot product operations for getting the output matrix
    // outputs 3 ciphertexts that represents the 3 rows of output matrix
    std::vector<helib::Ctxt> output_ct_vec1;
    for(int i = 0; i < 3; i++) {
        helib::Ctxt ct(*encryptor.getPublicKey());
        for (int j = 0; j < 3; j++) {
            helib::Ctxt product_ctxt(vec_ctxt1[i]);
            product_ctxt.multiplyBy(vec_ctxt2[j]);
            helib::totalSums(*encryptor.getEncryptedArray(), product_ctxt);

            helib::Ptxt <helib::BGV> ptxt(*(encryptor.getContext()));
            ptxt[j] = 1;
            product_ctxt.multByConstant(ptxt);
            ct += product_ctxt;
       }
        output_ct_vec1.push_back(ct);
    }

    std::cout << "Output matrix: " << std::endl;
    for(int i = 0; i < 3; i++) {
        debug(encryptor, output_ct_vec1[i], "   ", true);
    }
}
