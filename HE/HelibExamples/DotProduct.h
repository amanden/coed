//
// Created by Aswathi Kunnumbrath Manden(ak3793)
// 10/14/20
// The program homomorphically evaluate dot product of two 5x5 matrices
//

#ifndef HELIBEXAMPLES_DOTPRODUCT_H
#define HELIBEXAMPLES_DOTPRODUCT_H

#include "Encryptor.h"

class DotProduct {
public:
    // Performs the dot product homomorphically on encrypted data
    static void dot_product(std::vector<long> v_1, std::vector<long> v_2);

private:
    // This function is used to print the input vectors
    static void print_input_vector(const std::string &msg, const std::vector<long> &vec);

    // This function is used by debug function to print the plaintext vectors
    static void print_vector(const std::string &msg, const std::vector<long> &vec, bool space=false);

    //This function is used for debugging. Decryptes the encrypted ciphertext and prints the plaintext.
    static void debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space=false);

    // encodes the plaintexts with input vector values
    static void fill_plaintext_with_numbers(std::vector<long> &plaintext, std::vector<long> &v);
};


#endif //HELIBEXAMPLES_DOTPRODUCT_H
