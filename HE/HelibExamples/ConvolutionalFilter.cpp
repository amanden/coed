//
// Created by Aswathi on 10/14/20.
// This program computes the 4x4 feature map homomorphically using 6x6 input matrix and 3x3 filter matrix.
//

#include "ConvolutionalFilter.h"
#include <iostream>
#include "Util.h"
//#include <omp.h>

void
ConvolutionalFilter::print_vector(const std::string &msg, const std::vector<long> &vec, bool space) {
    std::cout << msg;
    for (auto &slot : vec) {
        std::cout << slot;
        if (space)
            std::cout << " ";
    }
    std::cout << std::endl;
}

// prints the input vector
void
ConvolutionalFilter::print_input_vector(const std::string &msg, const std::vector<std::vector<long>> &vec) {
    std::cout << msg << std::endl;
    for(int i = 0; i < vec.size(); i++) {
        for(int j = 0; j < vec[0].size(); j++){
            std::cout << vec[i][j]<< " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// prints the feature_map
void
ConvolutionalFilter::print_featureMap_vector(const COED::Encryptor &encryptor, const std::vector<helib::Ctxt> feature_map) {
    std::cout << "Feature map: " << std::endl;
    int a= 0;
    for(int i = 0; i < 16; i++) {
        std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
        encryptor.getEncryptedArray()->decrypt(feature_map[i], *encryptor.getSecretKey(), plaintext);
        std::cout << plaintext[0] << " " ;
        if(a == 3 ) {
            std::cout << " " << std::endl;
            a = 0;
        }else a++;
    }
}

void
ConvolutionalFilter::debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space) {
    std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
    encryptor.getEncryptedArray()->decrypt(ctxt, *encryptor.getSecretKey(), plaintext);
    print_vector(msg, plaintext, space);
}

/**
 * Computes the 4x4 feature map homomorphically using 6x6 input matrix and 3x3 filter matrix.
 * input matrix is encoded into 16 ciphertexts and performs 16 dot products.
 * filter matrix encoded into 1 ciphertext.
 * @param in_vec input matrix
 * @param fltr_vec  filter matrix
 */
void
ConvolutionalFilter::filter_product(std::vector<std::vector<long>> &in_vec, std::vector<std::vector<long>> &fltr_vec) {
    unsigned long plaintext_prime_modulus = 53;
    unsigned long phiM = 1989;
    unsigned long lifting = 1;
    unsigned long numOfBitsOfModulusChain = 512;
    unsigned long numOfColOfKeySwitchingMatrix = 2;

    COED::Encryptor encryptor("/tmp/sk.txt", "/tmp/pk.txt",
                              plaintext_prime_modulus,
                              phiM,
                              lifting,
                              numOfBitsOfModulusChain,
                              numOfColOfKeySwitchingMatrix);
    COED::Util::info("Finished creating encryptor.");

    print_input_vector("Input data: ", in_vec);
    print_input_vector("Filter: ", fltr_vec);

    // Encoding the filter 3x3 matrix into 1 plaintext
    std::vector<long> fltr_vec_ptxt(encryptor.getEncryptedArray()->size());
    int k = 0;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            fltr_vec_ptxt[k] = fltr_vec[i][j];
            k++;
        }
    }
    // filling the remaining slotes with 0s
    for(int i=k; i<fltr_vec_ptxt.size(); ++i) {
        fltr_vec_ptxt[i] = 0;
    }
    helib::Ctxt fltr_vec_ctxt(*encryptor.getPublicKey());
    encryptor.getEncryptedArray()->encrypt(fltr_vec_ctxt, *encryptor.getPublicKey(), fltr_vec_ptxt);

    std::vector<std::vector<long>> in_vec_pt;
    // Encoding each 3x3 matrix in input matrix to a plaintext, generating 16 plaintexts
    for(int y = 0; y < 4; y++) {
        for (int x = 0; x < 4; x++) {
            std::vector<long> in_pt(encryptor.getEncryptedArray()->size());
            k=0;
            for(int i = y; i < y+3; i++) {
                for (int j = x; j < x+3; j++) {
                    in_pt[k++] = in_vec[i][j];
                }
            }
            in_vec_pt.push_back(in_pt);
        }
    }

    std::vector<helib::Ctxt> feature_map;
    helib::Ctxt output_ctxt(*encryptor.getPublicKey());
    // find the dot product of 16 input ciphertexts with filter
    // store the resulting output ciphertexts in a vector( feature_map )
    for(int i = 0; i < 16; i++) {
        helib::Ctxt ct(*encryptor.getPublicKey());
        encryptor.getEncryptedArray()->encrypt(ct, *encryptor.getPublicKey(), in_vec_pt[i]);

        helib::Ctxt ct_cpy(ct);
        ct_cpy.multiplyBy(fltr_vec_ctxt);
        helib::totalSums(*encryptor.getEncryptedArray(), ct_cpy);

        feature_map.push_back(ct_cpy);

        helib::Ptxt <helib::BGV> ptxt(*(encryptor.getContext()));
        ptxt[i] = 1;
        ct_cpy.multByConstant(ptxt);
        output_ctxt += ct_cpy;
    }

    // print the feature_map
    print_featureMap_vector(encryptor, feature_map);

    // Single output ciphertext
    std::cout << "Output ciphertext: " << std::endl;
    debug(encryptor, output_ctxt, "   ", true);
}


/**
 * Computes the 4x4 feature map homomorphically using 6x6 input matrix and 3x3 filter matrix.
 * input matrix is encoded into 1 ciphertext and performs 16 dot products after rotating the ciphertexts 16 times.
 * filter matrix encoded into 1 ciphertext.
 * @param in_vec input matrix
 * @param fltr_vec  filter matrix
 */
void
ConvolutionalFilter::filter_product_with_one_ct(std::vector<std::vector<long>> &in_vec, std::vector<std::vector<long>> &fltr_vec) {
    unsigned long plaintext_prime_modulus = 53;
    unsigned long phiM = 1989;
    unsigned long lifting = 1;
    unsigned long numOfBitsOfModulusChain = 512;
    unsigned long numOfColOfKeySwitchingMatrix = 2;

    COED::Encryptor encryptor("/tmp/sk.txt", "/tmp/pk.txt",
                              plaintext_prime_modulus,
                              phiM,
                              lifting,
                              numOfBitsOfModulusChain,
                              numOfColOfKeySwitchingMatrix);
    COED::Util::info("Finished creating encryptor.");

    print_input_vector("Input data: ", in_vec);
    print_input_vector("Filter: ", fltr_vec);

    // Encoding the filter 3x3 matrix into 1 plaintext
    std::vector<long> fltr_vec_ptxt(encryptor.getEncryptedArray()->size());
    int k = 0;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            fltr_vec_ptxt[k] = fltr_vec[i][j];
            k++;
        }
    }
    // filling the remaining slotes with 0s
    for(int i=k; i<fltr_vec_ptxt.size(); ++i) {
        fltr_vec_ptxt[i] = 0;
    }
    // Encrypt the plaintext filter using the public_key
    helib::Ctxt fltr_vec_ctxt(*encryptor.getPublicKey());
    encryptor.getEncryptedArray()->encrypt(fltr_vec_ctxt, *encryptor.getPublicKey(), fltr_vec_ptxt);

    std::vector<std::vector<long>> in_vec_pt;
    std::vector<long> input_ptxt(encryptor.getEncryptedArray()->size());

    // Encoding each 3x3 matrix in input matrix to a plaintext, generating 1 plaintext
    int a= 0;
    for(int y = 0; y < 4; y++) {
        for (int x = 0; x < 4; x++) {
            for(int i = y; i < y+3; i++) {
                for (int j = x; j < x+3; j++) {
                    input_ptxt[a++] = in_vec[i][j];
                }
            }
        }
    }

    // Encrypt the input plaintext using the public_key
    helib::Ctxt input_ctxt(*encryptor.getPublicKey());
    encryptor.getEncryptedArray()->encrypt(input_ctxt, *encryptor.getPublicKey(), input_ptxt);

    // rotate the ciphertext 16 times and find the dot product each time
    // store the resulting output ciphertexts in a vector( feature_map )
    std::vector<helib::Ctxt> feature_map;
    helib::Ctxt output_ctxt(*encryptor.getPublicKey());
    for(int i = 0; i < 16; i++) {
        helib::Ctxt ct_cpy(input_ctxt);
        encryptor.getEncryptedArray()->rotate(ct_cpy, -i * 9);
        ct_cpy.multiplyBy(fltr_vec_ctxt);
        helib::totalSums(*encryptor.getEncryptedArray(), ct_cpy);
        feature_map.push_back(ct_cpy);

        helib::Ptxt <helib::BGV> ptxt(*(encryptor.getContext()));
        ptxt[i] = 1;
        ct_cpy.multByConstant(ptxt);
        output_ctxt += ct_cpy;
    }
    // print the feature_map
    print_featureMap_vector(encryptor, feature_map);

    // Single output ciphertext
    std::cout << "Output ciphertext: " << std::endl;
    debug(encryptor, output_ctxt, "   ", true);

}
