//
// Created by Aswathi on 10/14/20.
// This program computes the 4x4 feature map homomorphically using 6x6 input matrix and 3x3 filter matrix.
//

#ifndef HELIBEXAMPLES_CONVOLUTIONALFILTER_H
#define HELIBEXAMPLES_CONVOLUTIONALFILTER_H

#include "Encryptor.h"

class ConvolutionalFilter {
public:
    static void filter_product(std::vector<std::vector<long>> &in_vec, std::vector<std::vector<long>> &v_2);
    static void filter_product_with_one_ct(std::vector<std::vector<long>> &in_vec, std::vector<std::vector<long>> &fltr_vec);

private:
    static void print_input_vector(const std::string &msg, const std::vector<std::vector<long>> &vec);
    static void print_vector(const std::string &msg, const std::vector<long> &vec, bool space=false);
    static void debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space=false);
    static void print_featureMap_vector(const COED::Encryptor &encryptor, const std::vector<helib::Ctxt> feature_map);
};


#endif //HELIBEXAMPLES_CONVOLUTIONALFILTER_H
