# Project 1

The new files added in HelibExamples for this project are:

```bash
HECalculator.cpp
HECalculator.h

DotProduct.cpp
DotProduct.h

MatrixMultiplication.cpp
MatrixMultiplication.h

ConvolutionalFilter.cpp
ConvolutionalFilter.h
```



The program can be run from ../deps/bin/ as below:

* For task 1 decimal calculator,e.g,
```bash
./HElibExamples task1 2 5 multiply
```
User can specify the input as command line arguments.

or,if no argument is passed
```bash
./HElibExamples task1
```
If no argument is passed then user will be asked to input the numbers and operation( add, subtract, multiply).


* For task 2 dot product,e.g,
```bash
./HElibExamples task2 0 1 2 3 4 4 3 2 1 0
```
User can specify the input as command line arguments. The above arguments will create 2 
vectors { 0, 1, 2, 3, 4 } and { 4, 3, 2, 1, 0 }.

or, if no argument is passed
```bash
./HElibExamples task2 
```
The code will run with the hardcoded examples 
{ 0, 1, 2, 3, 4 } and { 4, 3, 2, 1, 0 }, 

* For task 3 matrix multiplication, the example is hardcoded in the code and user can run it as,
```bash
./HElibExamples task3
```
The code will run with the hardcoded example,
```bash
Matrix 1:
0, 1, 2
2, 1, 0
1, 0, 2 

Matrix 2:
2, 0, 1
1, 2, 2
1, 1, 0 
  ```

* For task 4, evaluation of convolutional filter, the example is hardcoded in the code and user can run it as,
```bash
./HElibExamples task4
```
The code will run with the hardcoded example,
```bash
Input data(6x6):
1, 1, 1, 0, 0, 0 
0, 1, 1, 1, 0, 0 
0, 0, 1, 1, 1, 0 
0, 0, 0, 1, 1, 1 
0, 0, 0, 1, 1, 0
0, 0, 1, 1, 0, 0

Filter(3x3):
1, 0, 1
0, 1, 0
1, 0, 1
  ```

