//
// Created by Aswathi on 10/13/20.
// The implementation of a simple decimal calculator. Perform the requested homomorphic computation.
//

#ifndef HELIBEXAMPLES_HECALCULATOR_H
#define HELIBEXAMPLES_HECALCULATOR_H

#include "Encryptor.h"

class HECalculator {
    public:
        static void calculator(int a, int b, std::string optr);

    private:
        // functions for debugging
        static void print_vector(const std::string &msg, const std::vector<long> &vec, bool space=false);
        static void debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space=false);
};


#endif //HELIBEXAMPLES_HECALCULATOR_H
