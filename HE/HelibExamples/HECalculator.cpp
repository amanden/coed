//
// Created by Aswathi on 10/13/20.
// The implementation of a simple decimal calculator. Perform the requested homomorphic computation.
//

#include "HECalculator.h"
#include <iostream>
#include "Util.h"

void
HECalculator::print_vector(const std::string &msg, const std::vector<long> &vec, bool space) {
    std::cout << msg;
    for (auto &slot : vec) {
        std::cout << slot;
        if (space)
            std::cout << " ";
    }
    std::cout << std::endl;
}

void
HECalculator::debug(const COED::Encryptor &encryptor, const helib::Ctxt &ctxt, const std::string &msg, bool space) {
    std::vector<long> plaintext(encryptor.getEncryptedArray()->size());
    encryptor.getEncryptedArray()->decrypt(ctxt, *encryptor.getSecretKey(), plaintext);
    print_vector(msg, plaintext, space);
}

/**
 * Implementation of calculator.
 * Performs homomorphic addition, subtraction, and multiplication on encrypted numbers.
 * @param a input 1
 * @param b input 2
 * @param optr operation
 */
void
HECalculator::calculator(int a, int b, std::string optr) {
    unsigned long plaintext_prime_modulus = 53;
    unsigned long phiM = 83;
    unsigned long lifting = 1;
    unsigned long numOfBitsOfModulusChain = 512;
    unsigned long numOfColOfKeySwitchingMatrix = 2;

    COED::Encryptor encryptor("/tmp/sk.txt", "/tmp/pk.txt",
                              plaintext_prime_modulus,
                              phiM,
                              lifting,
                              numOfBitsOfModulusChain,
                              numOfColOfKeySwitchingMatrix);
    COED::Util::info("Finished creating encryptor.");
    // Create a vector of long with nslots elements
    helib::Ptxt <helib::BGV> ptxt1(*(encryptor.getContext()));
    helib::Ptxt <helib::BGV> ptxt2(*(encryptor.getContext()));

    // Set it with numbers 0..nslots - 1
    ptxt1[0] = a;  // setting first slot with input no. 1
    for (int i = 1; i < ptxt1.size(); i++) {
        ptxt1[i] = 0;  // setting remaining slotes with 0
    }

    ptxt2[0] = b;
    for (int i = 1; i < ptxt2.size(); i++) {
        ptxt2[i] = 0;
    }

    // Print the plaintext
    std::cout << "Plaintext number 1: " << ptxt1 << std::endl;
    std::cout << "Plaintext number 2: " << ptxt2 << std::endl;

    // Create ciphertexts
    helib::Ctxt ctxt1(*(encryptor.getPublicKey()));
    helib::Ctxt ctxt2(*(encryptor.getPublicKey()));

    // Encrypt the plaintexts using the public_key
    encryptor.getPublicKey()->Encrypt(ctxt1, ptxt1);
    encryptor.getPublicKey()->Encrypt(ctxt2, ptxt2);

    // applying the input operation on ciphertexts
    if (optr == "multiply"){
        ctxt1 *= ctxt2;
    }else if (optr == "add"){
        ctxt1 += ctxt2;
    }else if (optr == "subtract"){
        ctxt1 -= ctxt2;
    }else{
        std::cout << "Invalid operator : " << optr << std::endl;
        return;
    }
    debug(encryptor, ctxt1, "Result: ", true);
}
