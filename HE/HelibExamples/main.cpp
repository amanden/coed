//
// Aswathi Kunnumbrath Manden
// The main program of execution
//


#include <iostream>
#include "BasicExamples.h"
#include "HECalculator.h"
#include "DotProduct.h"
#include "MatrixMultiplication.h"
#include "ConvolutionalFilter.h"

void calculator_task1(int argc, char* argv[]){
    if (argc == 2) {
        int a, b;
        std::cout << " x =  " << std::endl;
        std::cin >> a;
        std::cout << " y =  " << std::endl;
        std::cin >> b;
        std::string op;
        std::cout << " operator =  " << std::endl;
        std::cin >> op;
        HECalculator::calculator(a, b, op);
    } else if (argc == 5) {
        int a = std::stoi(argv[2]);
        int b = std::stoi(argv[3]);
        HECalculator::calculator(a, b, argv[4]);
    } else {
        std::cout << "Invalid command line arguments " << std::endl;
    }
}
void dot_product_task2(int argc, char* argv[]){
    if (argc == 2) {
        std::vector<long> vect1{ 0, 1, 2, 3, 4 };
        std::vector<long> vect2{ 4, 3, 2, 1, 0 };
        DotProduct::dot_product(vect1, vect2);
    }else if (argc == 12) {
        std::vector<long> vect1, vect2;
        vect1.push_back(std::stol(argv[2])); vect1.push_back(std::stol(argv[3]));
        vect1.push_back(std::stol(argv[4])); vect1.push_back(std::stol(argv[5]));
        vect1.push_back(std::stol(argv[6])); vect2.push_back(std::stol(argv[7]));
        vect2.push_back(std::stol(argv[8])); vect2.push_back(std::stol(argv[9]));
        vect2.push_back(std::stol(argv[10])); vect2.push_back(std::stol(argv[11]));
        DotProduct::dot_product(vect1, vect2);
    }
    else {
        std::cout << "Invalid command line arguments " << std::endl;
    }
}

void matrix_multiplication_task3(int argc, char* argv[]){
    if (argc == 2) {
        std::vector<long> v1{ 0,1, 2 };
        std::vector<long> v2{ 2,1, 0 };
        std::vector<long> v3{ 1,0, 2 };

        std::vector<long> v4{2, 0, 1 };
        std::vector<long> v5{1 ,2, 2 };
        std::vector<long> v6{1, 1, 0 };

        std::vector<std::vector<long>> v_1;
        v_1.push_back(v1);v_1.push_back(v2);v_1.push_back(v3);
        std::vector<std::vector<long>> v_2;
        v_2.push_back(v4);v_2.push_back(v5);v_2.push_back(v6);

        MatrixMultiplication::matrix_product(v_1, v_2);
    }
}

void conv_filter_task4(int argc, char* argv[]) {
    if (argc == 2) {
        std::vector<long> v1{ 1,1,1,0,0,0 };
        std::vector<long> v2{ 0,1,1,1,0,0 };
        std::vector<long> v3{ 0,0,1,1,1,0 };
        std::vector<long> v4{ 0,0,0,1,1,1 };
        std::vector<long> v5{ 0,0,0,1,1,0 };
        std::vector<long> v6{ 0,0,1,1,0,0 };

        std::vector<std::vector<long>> in_v;
        in_v.push_back(v1);in_v.push_back(v2);in_v.push_back(v3);
        in_v.push_back(v4);in_v.push_back(v5);in_v.push_back(v6);

        std::vector<long> f1{1,0,1 };
        std::vector<long> f2{0,1,0 };
        std::vector<long> f3{1,0,1 };
        std::vector<std::vector<long>> fltr_v;
        fltr_v.push_back(f1);fltr_v.push_back(f2);fltr_v.push_back(f3);

        auto t1 = std::chrono::high_resolution_clock::now();
        ConvolutionalFilter::filter_product(in_v, fltr_v);
        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
        std::cout << "Duration with 16 ctxt :"<<duration<< " milliseconds"<< std::endl;

        t1 = std::chrono::high_resolution_clock::now();
        ConvolutionalFilter::filter_product_with_one_ct(in_v, fltr_v);
        t2 = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
        std::cout << "Duration with 1 ctxt :" << duration << " milliseconds"<< std::endl;

    }
}

int main(int argc, char* argv[] ) {
    std::cout << "Program Start!!!" << std::endl;

    if( argc >= 2 ){
        std::string my_arg = argv[1];
        if( my_arg.compare("task1") == 0) {
            calculator_task1(argc, argv);
        }
        else if( my_arg.compare("task2") == 0){
            dot_product_task2(argc, argv);
        }
        else if( my_arg.compare("task3") == 0){
            matrix_multiplication_task3(argc, argv);
        }
        else if( my_arg.compare("task4") == 0){
            conv_filter_task4(argc, argv);
        }else{
            std::cout << "Invalid command line arguments, please specify the task# " << std::endl;
        }
    }

    std::cout << "Program Finished!!!" << std::endl;
    return 0;
}
